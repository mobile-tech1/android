# VLPL_Pharma_App


## Project Overview

  
VLPL is a user-friendly and secure mobile application that aims to simplify the process of purchasing medicines online. The app will provide a convenient platform for users to order medicines.

  ## Objectives
  

 - `Seamless Medicine Ordering:` Enable users to browse and search for medicines easily, add them to their cart, and place orders hassle-free.
 - `Medication Information:` Provide detailed information about each medicine, including dosage, side effects, precautions, and possible drug interactions.
 - `Secure Payment Processing:` Implement a secure payment gateway to facilitate safe transactions.

  ## Features

 - `User Registration and Authentication:` Allow users to create accounts using email or mobile numbers and implement secure authentication mechanisms.
 - `Medicine Search and Filters:` Implement a robust search functionality to help users find medicines quickly. Provide filters to refine search results based on categories, brands, and prices.
 - `Shopping Cart:` Allow users to add medicines to their cart, review the items, and proceed to checkout.
 - `Order History:` Maintain a record of users' past orders for easy reference.
 - `Contact Support:` Provide a dedicated customer support channel for users to resolve queries and concerns.

  ## Technology Stack
  The App will be developed for the Android platform, utilizing the following technologies:
  - Programming Language: Kotlin v1.7.10
  - SDK: min27 and max33+
  - Database: PHPMyAdmin
  - Payment Gateway Integration: Razorpay 
  - Cloud Services: Firebase for  push notifications
  - UI/UX: Android XML layouts with Material Design guidelines
  - Project Architecture: MVVM, Data-Binding, View-Binding

  ## Requirement

 - `Android Studio:` Bumblebee | 2021.1.1+
